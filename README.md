Background
----------

Pit your best robot against the best in the world! 

![Enter a robocode competition and battle the world's best robots](http://www.ibm.com/developerworks/library/j-robocode2/fig4.gif)

Try new APIs in creating your robot team.  Learn advanced robot design strategies from the masters.

![Learn new APIs and strategies](http://www.ibm.com/developerworks/library/j-robocode2/fig2.gif)

Please see [Rock'em, sock'em Robocode: Round2](http://www.ibm.com/developerworks/library/j-robocode2/)  for detailed information on working with this code.

The code in this repository may be periodically updated.  Download the [latest distribution zip file here](https://bitbucket.org/singli/rockem-sockem-robocode-round-2/downloads/coderobo2_20140116.zip).

### UPDATE

Ready to rumble, direct loadable robots and teams are now available! The code is now compatible with Robocode 1.9.0.0 Beta 2.  Download the [code zip here](https://bitbucket.org/singli/rockem-sockem-robocode-round-2/downloads/coderobo2_20140116.zip).
